import { SEND_MESSAGE, RECIVE_MESSAGE, USER_LIST } from "./../actions/chat_actions";


let initialState = {
    messages: [],
    users: []
}

let chats = (state = initialState, action) => {
    switch (action.type) {
        case RECIVE_MESSAGE:
            state = { ...state, messages: [...state.messages, action.message] }
            break;
        case SEND_MESSAGE:
            state = { ...state }
            break;
        case USER_LIST:
            state = { ...state, users: action.users }
            break;
        default:
            break;
    }
    return state;
}

export { chats };
