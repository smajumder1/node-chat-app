import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/App';
import Chat from "./components/Chat";

import { BrowserRouter, Switch, Route } from "react-router-dom"
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";

import rootReducer from "./reducers";

const store = createStore(rootReducer, applyMiddleware(thunk));

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={App} />
                <Route path="/chat" component={Chat} />
            </Switch>
        </BrowserRouter>
    </Provider>

    , document.getElementById('root'));

