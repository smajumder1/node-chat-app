import openSocket from "socket.io-client";

export const SEND_MESSAGE = "SEND_MESSAGE";
export const RECIVE_MESSAGE = "RECIVE_MESSAGE";
export const USER_LIST = "USER_LIST";

export const socket = openSocket();

let sendText = mesasge => {
  socket.emit("createMessage", mesasge, () => {
    console.log("Got it");
  });
  return {
    type: SEND_MESSAGE,
    mesasge
  };
};

let getMessage = message => {
  return {
    type: RECIVE_MESSAGE,
    message
  };
};

let getUsers = users => {
  return {
    type: USER_LIST,
    users
  };
};
let getWelcomeMessage = msg => {
  return dispatch => {
    socket.on("welcomeMessage", msg => {
      dispatch(getMessage(msg));
    });
  };
};

let reciveMessage = () => dispatch => {
  socket.on("newMessage", message => {
    console.log(message);
    dispatch(getMessage(message));
  });
  socket.on("newLocationMessage", message => {
    console.log(message);
    dispatch(getMessage(message));
  });
};

let getUserList = () => dispatch => {
  socket.on("updateUserList", users => {
    console.log("users", users);
    dispatch(getUsers(users));
  });
};

export { sendText, reciveMessage, getWelcomeMessage, getUserList };
