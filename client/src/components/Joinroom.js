import React, { Component } from "react";
import { FormControl, ControlLabel } from "react-bootstrap";
import Cookies from "react-cookies";
import { Redirect } from "react-router-dom";

class Joinroom extends Component {
  state = {
    chatname: "",
    roomname: "",
    inputValidation: "",
    redirect: false
  };

  join = () => {
    Cookies.save("chatname", this.state.chatname, { path: "/" });
    Cookies.save("roomname", this.state.roomname, { path: "/" });
    this.setState({ redirect: true });
  };

  handleChange = (val, e) => {
    if (val === "chatname") {
      this.setState({ chatname: e.target.value });
    } else if (val === "chatroom") {
      this.setState({ roomname: e.target.value });
    }
  };

  handleFocus = (val, e) => {
    if (e.keyCode === 13) {
      if (val === "chatname") {
        this.chatRoom.focus();
      } else if (val === "chatroom") {
        this.joinRoom.focus();
      }
    }
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to="/chat" />;
    }
    return (
      <div className="joinroom-container">
        <div className="joinroom-modal">
          <h3 className="header">Chat</h3>
          <ControlLabel>Chat Name</ControlLabel>
          <FormControl
            inputRef={input => {
              this.chatName = input;
            }}
            type="text"
            placeholder="Enter Name"
            value={this.state.chatname}
            onChange={this.handleChange.bind(this, "chatname")}
            onKeyUp={this.handleFocus.bind(this, "chatname")}
          />
          <br />
          <ControlLabel>Chat Room</ControlLabel>
          <FormControl
            inputRef={input => {
              this.chatRoom = input;
            }}
            type="text"
            placeholder="Enter Chat room"
            value={this.state.roomname}
            onChange={this.handleChange.bind(this, "chatroom")}
            onKeyUp={this.handleFocus.bind(this, "chatroom")}
          />
          <br />
          <button
            ref={input => {
              this.joinRoom = input;
            }}
            className="btn btn-primary btn-block"
            onClick={this.join}
          >
            Join
          </button>
        </div>
      </div>
    );
  }
}

export default Joinroom;
