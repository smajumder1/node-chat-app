import React, { Component } from "react";
import Moment from "moment";
import Map from "google-map-react";

import { Marker } from "./Marker";

class MessageBubble extends Component {

    state = {
        isOpen: false
    }

    scrollToBottom = () => {
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    }

    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    toggleTime = () => {
        if (this.state.isOpen) {
            this.setState({ isOpen: false });
        }
        else {
            this.setState({ isOpen: true });
        }
    }

    render() {
        return (
            <li>
                {
                    this.props.msg.text ? <span onClick={this.toggleTime} className={this.props.msg.from !== this.props.name ? "msg-span left" : "msg-span right"}><b>{this.props.msg.from} :</b> {this.props.msg.text}</span>
                        : <span onClick={this.toggleTime} className={this.props.msg.from !== this.props.name ? "msg-span left" : "msg-span right"}><b>{this.props.msg.from} :</b>
                            <div style={{ height: '180px', width: '180px' }}>
                                <Map
                                    bootstrapURLKeys={{ key: 'AIzaSyA5xUKbxYWFZ9fgk26eWv1W1ectc2azjv8' }}
                                    defaultCenter={{ lat: parseFloat(this.props.msg.coords.split(',')[0]), lng: parseFloat(this.props.msg.coords.split(',')[1]) }}
                                    defaultZoom={15}
                                >
                                    <Marker lat={parseFloat(this.props.msg.coords.split(',')[0])} lng={parseFloat(this.props.msg.coords.split(',')[1])} />
                                </Map>
                            </div>

                            <a target='_blank' href={this.props.msg.url} className='map-url'>My Current Location</a></span>
                }
                <div
                    className={this.props.msg.from === this.props.name ? "time-right" : "time"}
                    style={this.state.isOpen ? { display: 'block' } : { display: 'none' }}
                >
                    {Moment(this.props.msg.created_at).format('h:mm a')}
                </div>
                <div className="clear" ref={(el) => { this.messagesEnd = el; }}></div>
            </li>
        );
    }
}


export default MessageBubble;