import React, { Component } from "react";
import { Button, FormControl, Grid, Row, Col, Panel } from "react-bootstrap";
import UserList from "./UserList";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Cookies from "react-cookies";

import Messagebubble from "./MessageBubble";

import "./../styles/index.css";
import {
  sendText,
  reciveMessage,
  getWelcomeMessage,
  socket
} from "./../actions/chat_actions";

class Chat extends Component {
  constructor() {
    super();

    this.state = {
      text: ""
    };
  }

  componentWillMount() {
    socket.emit(
      "join",
      {
        name: Cookies.load("chatname"),
        room: Cookies.load("roomname")
      },
      err => {
        if (err) {
          console.log(err);
        }
      }
    );

    this.props.reciveMessage();
    socket.on("disconnect", () => {
      console.log("Disconnected from server");
    });
  }

  sendText = () => {
    this.props.sendText({
      from: Cookies.load("chatname"),
      text: this.state.text
    });
    this.setState({
      text: ""
    });
    // e.preventDefault();
  };

  handleGeoLocation = () => {
    if (!navigator.geolocation) {
      return alert("GeoLocation is not supported");
    }

    navigator.geolocation.getCurrentPosition(
      postition => {
        socket.emit("createLocationMessage", {
          latitude: postition.coords.latitude,
          longitude: postition.coords.longitude
        });
      },
      err => {
        console.log(err);
        alert("Unable to get the location");
      }
    );
  };

  handleTextChange = e => {
    this.setState({
      text: e.target.value
    });
  };

  submitOnEnter = e => {
    if (e.keyCode === 13) {
      this.sendText();
    }
  };

  render() {
    return (
      <div className="">
        <Grid fluid>
          <Row className="show-grid">
            <Col xs={4} md={2} className="contact">
              <h3 className="people-header"> Peoples </h3>
              <UserList />
            </Col>
            <Col xs={8} md={10} className="chat-container">
              <Panel className="chat-box">
                <Panel.Body>
                  <ul>
                    {this.props.chats.messages.map((msg, index) => {
                      return (
                        <Messagebubble
                          msg={msg}
                          key={index}
                          name={Cookies.load("chatname")}
                        />
                      );
                    })}
                  </ul>
                </Panel.Body>
              </Panel>
              <Row>
                <Col xs={9} m={9}>
                  <FormControl
                    type="text"
                    placeholder="Message"
                    value={this.state.text}
                    onChange={this.handleTextChange}
                    style={{ width: "100%" }}
                    onKeyUp={this.submitOnEnter}
                  />
                </Col>
                <Col xs={3} m={3}>
                  <Button
                    className="btn btn-primary"
                    onClick={this.sendText}
                    style={{ width: "40%" }}
                  >
                    Send
                  </Button>{" "}
                  <Button
                    className="btn btn-default"
                    onClick={this.handleGeoLocation}
                    style={{ width: "58%" }}
                  >
                    Send Location
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

let mapStateToProps = state => {
  return {
    chats: state.chats
  };
};

let mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      sendText,
      reciveMessage,
      getWelcomeMessage
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
