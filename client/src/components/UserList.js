import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getUserList } from "./../actions/chat_actions";

class UserList extends Component {
  componentWillMount() {
    this.props.getUserList();
  }
  render() {
    return (
      <div>
        {this.props.users.map((user, index) => {
          return (
            <div key={index} className="user-wrapper">
              <div className="user-image" />
              <div className="user-name">{user.name}</div>
              <div className="user-status" />
            </div>
          );
        })}
      </div>
    );
  }
}

let mapStateToProps = state => {
  return {
    users: state.chats.users
  };
};

let mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getUserList
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);
