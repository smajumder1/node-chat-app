import React, { Component } from 'react';

//import Chat from "./Chat";
import JoinRoom from "./Joinroom";

import "./../styles/index.css";

import { socket, getWelcomeMessage } from "./../actions/chat_actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

class App extends Component {

	_initSocket() {
		socket.on('connect', () => {
			console.log('Connected to server');
		});
		//this.props.getWelcomeMessage();
	}

	componentWillMount() {
		this._initSocket();

	}

	render() {
		return (
			<JoinRoom />
		);
	}

}
let mapDispatchToProps = (dispatch) => {
	return bindActionCreators({ getWelcomeMessage }, dispatch);
}

export default connect(null, mapDispatchToProps)(App);
