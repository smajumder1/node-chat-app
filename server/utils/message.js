const moment = require('moment');

let generateMessage = (from, text, coords) => {
	return {
		from,
		text,
		created_at: moment().valueOf()
	};
};

let generateLocationMessage = (from, coords) => {
	return {
		from,
		url: `http://www.google.com/maps?q=${coords}`,
		created_at: moment().valueOf(),
		coords
	};
}

module.exports = { generateMessage, generateLocationMessage };
