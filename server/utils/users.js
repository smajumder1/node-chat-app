class Users {
    constructor() {
        this.users = [];
    }

    addUser(id, name, room) {
        let user = { id, name, room }
        this.users.push(user);
        return user;
    }

    removeUser(id) {
        let deletedUser = this.getUser(id)
        if (deletedUser) {
            this.users = this.users.filter(user => user.id !== id);
        }
        return deletedUser;
    }

    getUser(id) {
        return this.users.filter(user => user.id === id)[0]
    }

    getUserList(room) {
        let users = this.users.filter(user => user.room === room);
        let userNames = users.map(user => user.name)
        return users;
    }
}

module.exports = { Users };