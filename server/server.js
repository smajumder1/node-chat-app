const path = require("path");
const express = require("express");
const http = require("http");
const socketIO = require("socket.io");
const cors = require("cors");

let { generateMessage, generateLocationMessage } = require("./utils/message");
let { Users } = require("./utils/users");

let app = express();
let server = http.createServer(app);
let io = socketIO(server, { pingTimeout: 60 * 60 * 24 });
let user = new Users();
const publicPath = path.join(__dirname, "../client/build");

app.use(express.static(publicPath));
app.use(cors({ origin: "*" }));
let PORT = process.env.PORT || 5000;

io.on("connection", socket => {
  console.log("New user connected");

  socket.on("join", (params, callback) => {
    socket.join(params.room);
    user.removeUser(socket.id);
    user.addUser(socket.id, params.name, params.room);
    console.log(user.getUserList(params.room));
    io.to(params.room).emit("updateUserList", user.getUserList(params.room));

    socket.emit("newMessage", generateMessage("Admin", "Welcome to chat app"));

    socket.broadcast
      .to(params.room)
      .emit("newMessage", generateMessage("Admin", `${params.name} joined`));

    console.log("Braodcast fired");
    callback();
  });
  socket.on("createMessage", (message, callback) => {
    console.log("createmsg", message);
    let users = user.getUser(socket.id);
    io
      .to(users.room)
      .emit("newMessage", generateMessage(message.from, message.text));
    callback();
  });

  socket.on("createLocationMessage", coords => {
    let users = user.getUser(socket.id);
    io
      .to(users.room)
      .emit(
        "newLocationMessage",
        generateLocationMessage(
          users.name,
          `${coords.latitude},${coords.longitude}`
        )
      );
  });

  socket.on("disconnect", () => {
    let users = user.removeUser(socket.id);
    console.log(users);
    if (users) {
      io.to(users.room).emit("updateUserList", user.getUserList(users.room));
      io
        .to(users.room)
        .emit(
          "newMessage",
          generateMessage("Admin", `${users.name} has been disconnected`)
        );
      console.log(`${users.name} disconnected`);
    }
  });
});

server.listen(PORT, () => {
  console.log("App started on port:", PORT);
});
